import torch
from torch import nn

class DistMaps(torch.nn.Module):
    def __init__(self, norm_radius=5, spatial_scale=1.0, use_disks=True):
        super().__init__()
        self.spatial_scale = spatial_scale
        self.norm_radius = norm_radius
        self.use_disks = use_disks

    def get_coord_features(self, points, rows, cols):
        num_points = points.shape[1] // 2
        points = points.view(-1, points.size(2))
        points, _ = torch.split(points, [2, 1], dim=1)

        invalid_points = torch.max(points, dim=1, keepdim=False)[0] < 0
        row_array = torch.arange(
            start=0,
            end=rows,
            step=1,
            dtype=torch.float32,
            device=points.device,
        )
        col_array = torch.arange(
            start=0,
            end=cols,
            step=1,
            dtype=torch.float32,
            device=points.device,
        )

        coord_rows, coord_cols = torch.meshgrid(
            row_array,
            col_array,
            indexing="ij",
        )
        coords = torch.stack((coord_rows, coord_cols), dim=0)
        coords = coords.unsqueeze(0).repeat(points.size(0), 1, 1, 1)

        add_xy = points * self.spatial_scale
        add_xy = add_xy.view(points.size(0), points.size(1), 1, 1)
        coords.add_(-add_xy)
        if not self.use_disks:
            coords.div_(self.norm_radius * self.spatial_scale)
        coords.mul_(coords)

        coords[:, 0] += coords[:, 1]
        coords = coords[:, :1]

        coords[invalid_points, :, :, :] = 1e6

        coords = coords.view(-1, num_points, 1, rows, cols)
        coords = coords.min(dim=1)[0]  # -> (bs * num_masks * 2) x 1 x h x w
        coords = coords.view(-1, 2, rows, cols)

        if self.use_disks:
            coords = (coords <= (self.norm_radius * self.spatial_scale) ** 2).float()
        else:
            coords.sqrt_().mul_(2).tanh_()

        return coords

    def forward(self, x, coords):
        return self.get_coord_features(coords, x.shape[2], x.shape[3])
