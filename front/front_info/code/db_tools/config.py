import os

host = os.environ['DB_HOST']
user = os.environ['POSTGRES_USER']
password = os.environ['POSTGRES_PASSWORD']
db_name = os.environ['POSTGRES_DB']
port = os.environ['DB_PORT']

DB_PATH = f'postgresql+psycopg2://{user}:{password}@{host}:{port}/{db_name}'