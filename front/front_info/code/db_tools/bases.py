from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy import Table, Column, Integer, String, Boolean, ARRAY, MetaData
from sqlalchemy import text, insert, update, select
from sqlalchemy import create_engine
from db_tools.config import DB_PATH

engine = create_engine(
    url=DB_PATH, 
    # echo=True
)

metadata_obj = MetaData()

info_table = Table(
    'info', 
    metadata_obj, 
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('nickname', String, primary_key=True),
    Column('images_order', ARRAY(String)),
    Column('last_image_number', Integer), 
    Column('last_crops_number', ARRAY(Integer)), 
    Column('class_type', Integer), 
    Column('click_type', Boolean)
)

images_parameters = Table(
    'images_parameters', 
    metadata_obj, 
    Column('image_name', String, primary_key=True),
    Column('crop_rows', Integer), 
    Column('crop_cols', Integer), 
    Column('rows', Integer), 
    Column('cols', Integer)
)

users_on_image = Table(
    'users_on_image', 
    metadata_obj, 
    Column('users_number', Integer, primary_key=True),
    Column('set_of_images', HSTORE)
)