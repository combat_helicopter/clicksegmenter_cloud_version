from sqlalchemy import text, insert, update, select
from db_tools.bases import metadata_obj, engine, info_table, images_parameters, users_on_image

from pydantic import BaseModel


class Item(BaseModel):
    id: int
    nickname: str
    images_order: list[str]
    last_image_number: int
    last_crops_number: list[int]
    class_type: int
    click_type: bool

##############################################
############### CREATE TABLES ################
##############################################
def create_tables(reset=False):
    global engine
    if reset:
        metadata_obj.drop_all(engine)
    metadata_obj.create_all(engine)


##############################################
################## SELECTS ###################
##############################################
def get_nicknames_db():
    global engine
    with engine.connect() as conn:
        statement = select(info_table.c['nickname'])
        res = conn.execute(statement)
        return res.all()

def nickname_already_exists_db(nickname):
    res = get_nicknames_db()
    return (nickname, ) in res

def get_user_info_db(nickname):
    global engine
    with engine.connect() as conn:
        statement = select(info_table).filter_by(nickname=nickname)
        res = conn.execute(statement)
        res = res.one()

        res = Item(
            id=res[0],
            nickname=res[1],
            images_order=res[2],
            last_image_number=res[3], 
            last_crops_number=res[4], 
            class_type=res[5], 
            click_type=res[6]
        )

        return res

def get_image_name_db_user(nickname, number):
    global engine
    with engine.connect() as conn:
        statement = select(info_table.c.images_order).filter_by(nickname=nickname)
        res = conn.execute(statement).one()[0]

        return res[number]

def get_db_size_db(db_name):
    global engine
    with engine.connect() as conn:
        statement = text(f'''SELECT count(*) FROM {db_name};''')
        res = conn.execute(statement).scalar()
        return res

def get_images_parameters_db():
    global engine
    with engine.connect() as conn:
        statement = select(images_parameters)
        res = conn.execute(statement).fetchall()
        res = {imgn: (cr, cc, r, c) for imgn, cr, cc, r, c in res}

        return res

def get_uoi_db():
    global engine
    with engine.connect() as conn:
        statement = select(users_on_image)
        res = conn.execute(statement).fetchall()
        res = {users_number: set(set_of_images) for users_number, set_of_images in res}
        return res

def get_ip_db(image_name=None):
    global engine
    with engine.connect() as conn:

        if image_name is None:
            statement = select(images_parameters)
            res = {imgn: (cr, cc, r, c) for imgn, cr, cc, r, c in conn.execute(statement).fetchall()}
        else:
            statement = select(images_parameters).filter_by(image_name=image_name)
            res = conn.execute(statement).one()[1:]

        return res

##############################################
################## UPDATES ###################
##############################################
def append_image_db(nickname, image_name):
    global engine
    with engine.connect() as conn:
        statement = text('''
            UPDATE info SET images_order = array_append(images_order, :image_name) WHERE nickname=:nickname;
            UPDATE info SET last_crops_number = array_append(last_crops_number, 0) WHERE nickname=:nickname;
            ''')
        statement = statement.bindparams(image_name=image_name, nickname=nickname)
        conn.execute(statement)
        conn.commit()

def change_current_image_number_db_user(nickname, number):
    global engine
    with engine.connect() as conn:
        statement = (
            update(info_table)
            .values(last_image_number=number)
            .filter_by(nickname=nickname)
            )
        
        conn.execute(statement)
        conn.commit()

def change_current_click_type_db_user(nickname, click_type):
    global engine
    with engine.connect() as conn:
        statement = (
            update(info_table)
            .values(click_type=click_type)
            .filter_by(nickname=nickname)
            )
        
        conn.execute(statement)
        conn.commit()

def change_current_class_type_db_user(nickname, class_type):
    global engine
    with engine.connect() as conn:
        statement = (
            update(info_table)
            .values(class_type=class_type)
            .filter_by(nickname=nickname)
            )
        
        conn.execute(statement)
        conn.commit()

def change_current_crop_number_db_user(nickname, image_number, crop_number):
    global engine
    with engine.connect() as conn:
        statement = text('''
            UPDATE info SET last_crops_number[:image_number] = :crop_number WHERE nickname=:nickname;
            ''')
        statement = statement.bindparams(image_number=image_number + 1, crop_number=crop_number, nickname=nickname)

        conn.execute(statement)
        conn.commit()

def uoi_add_image_db(users_number, image_name):
    global engine
    with engine.connect() as conn:
        statement = text('''
            UPDATE users_on_image
            SET set_of_images = set_of_images || hstore(:image_name, '')
            WHERE users_number = :users_number;
            ''')
        statement = statement.bindparams(users_number=users_number, image_name=image_name)

        conn.execute(statement)
        conn.commit()


def uoi_delete_image_db(users_number, image_name):
    global engine
    with engine.connect() as conn:
        statement = text('''
            UPDATE users_on_image
            SET set_of_images = delete(set_of_images, :image_name)
            WHERE users_number = :users_number;
            ''')
        statement = statement.bindparams(users_number=users_number, image_name=image_name)

        conn.execute(statement)
        conn.commit()

def change_current_image_parameters_ip_db(image_name, cr, cc, r, c):
    global engine
    with engine.connect() as conn:
        statement = text('''
            UPDATE images_parameters 
            SET crop_rows = :cr,
                crop_cols = :cc,
                rows = :r,
                cols = :c 
            WHERE image_name=:image_name;
            ''')
        statement = statement.bindparams(image_name=image_name, cr=cr, cc=cc, r=r, c=c)

        conn.execute(statement)
        conn.commit()

##############################################
################ INSERT ROW ##################
##############################################
def info_insert_row(nickname, images_order=[], last_image_number=-1, last_crops_number=[], class_type=0, click_type=True):
    global engine
    with engine.connect() as conn:
        statement = insert(info_table).values(
            [
                {
                    'nickname': nickname, 
                    'images_order': images_order, 
                    'last_image_number': last_image_number, 
                    'last_crops_number': last_crops_number,
                    'class_type': class_type, 
                    'click_type': click_type
                }, 
            ]
        )
        conn.execute(statement)
        conn.commit()

def add_user_db(nickname):
    info_insert_row(nickname)

def uoi_insert_row_db(set_of_images={}):
    global engine
    users_number = get_db_size_db('users_on_image')
    with engine.connect() as conn:
        statement = insert(users_on_image).values(
            [
                {
                    'users_number': users_number,
                    'set_of_images': set_of_images
                }, 
            ]
        )
        conn.execute(statement)
        conn.commit()

def ip_insert_row_db(image_name, cr=None, cc=None, r=None, c=None):
    global engine
    with engine.connect() as conn:
        statement = insert(images_parameters).values(
            [
                {
                    'image_name': image_name,
                    'crop_rows': cr,
                    'crop_cols': cc,
                    'rows': r,
                    'cols': c
                }, 
            ]
        )
        conn.execute(statement)
        conn.commit()






# -- CREATE TABLE USERS_NUMBER (
# -- 	users_number INTEGER primary key,
# -- 	attr hstore
# -- );

# -- INSERT INTO users_number (users_number, attr) 
# -- VALUES 
# --   (
# --     0, 
# --     '"img1" => "",
# --      "img2" => ""'
# --   );

# -- INSERT INTO users_number (users_number, attr) 
# -- VALUES 
# --   (
# --     1, 
# --     '"img3" => "",
# --      "img4" => ""'
# --   );

# -- UPDATE users_number
# -- SET attr = attr || '"img5"=>""' :: hstore
# -- WHERE users_number = 1;

# -- UPDATE users_number 
# -- SET attr = delete(attr, 'img0')
# -- WHERE users_number = 0;

# -- SELECT users_number, hstore_to_json (attr) json
# -- FROM users_number;


