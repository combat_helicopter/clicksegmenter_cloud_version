import os
import sys

from shutil import rmtree
from pages.preprocessing.cut_photos import cut_photos
from config import SIDE_SIZE, RESET_ALL

from db_tools.tools import create_tables
from db_tools.tools import (
    get_db_size_db, uoi_insert_row_db, get_ip_db, ip_insert_row_db, uoi_add_image_db
)

from time import sleep
print('Therapeutic downtime: 5', file=sys.stderr)
sleep(1)
print('Therapeutic downtime: 4', file=sys.stderr)
sleep(1)
print('Therapeutic downtime: 3', file=sys.stderr)
sleep(1)
print('Therapeutic downtime: 2', file=sys.stderr)
sleep(1)
print('Therapeutic downtime: 1', file=sys.stderr)
sleep(1)

DISKS_PATH = os.path.join(os.environ['DISKS'], 'disks')
DISK_FULL_IMAGES_PATH = os.path.join(os.environ['DISKS'], 'disk_full_images')
DISK_FULL_IMAGES_VIEW_PATH = os.path.join(os.environ['DISKS'], 'disk_full_images_view')
DISK_CROPS_PATH = os.path.join(os.environ['DISKS'], 'disk_crops')
REAL_FULL_IMAGES_PATH = os.environ['REAL_FULL_IMAGES_PATH']

PERSONAL_DISK_NAMES = ['disk_masks', 'disk_points', 'disk_masks_view']

print('Создаю db...', file=sys.stderr)
create_tables(reset=RESET_ALL)

print('Создаю папки...', file=sys.stderr)
if RESET_ALL:
    rmtree(DISKS_PATH, ignore_errors=True)
    rmtree(DISK_FULL_IMAGES_PATH, ignore_errors=True)
    rmtree(DISK_FULL_IMAGES_VIEW_PATH, ignore_errors=True)
    rmtree(DISK_CROPS_PATH, ignore_errors=True)

os.makedirs(DISKS_PATH, 0o777, exist_ok=True)
os.makedirs(DISK_FULL_IMAGES_PATH, 0o777, exist_ok=True)
os.makedirs(DISK_FULL_IMAGES_VIEW_PATH, 0o777, exist_ok=True)
os.makedirs(DISK_CROPS_PATH, 0o777, exist_ok=True)

if RESET_ALL:
    print('Обрабатываю изображения...', file=sys.stderr)
    cut_photos(REAL_FULL_IMAGES_PATH, DISK_FULL_IMAGES_PATH, SIDE_SIZE)

print('Запоминаю имена...', file=sys.stderr)
SUFFIXES = ['jpg', 'jpeg', 'png', 'JPG', 'JPEG']
IMAGE_NAMES = [
    imgn for imgn in os.listdir(DISK_FULL_IMAGES_PATH) 
    if imgn.split('.')[-1] in SUFFIXES
]

users_number = get_db_size_db('users_on_image')
if 0 == users_number:
    uoi_insert_row_db(set_of_images={imgn:'' for imgn in IMAGE_NAMES})

ip_dict = get_ip_db()
for imgn in IMAGE_NAMES:
    if imgn not in ip_dict:
        ip_insert_row_db(imgn)
        uoi_add_image_db(0, imgn)

print('setup done!', file=sys.stderr)