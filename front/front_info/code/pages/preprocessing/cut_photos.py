import os
import sys
from math import ceil

import numpy as np
from PIL import Image

SUFFIXES = ['jpg', 'jpeg', 'png', 'JPG', 'JPEG']

def cut_photos(disk_real_full_images_path, disk_new_full_images_path, side_size):
    for image_name in os.listdir(disk_real_full_images_path):
        print(image_name, file=sys.stderr)
        image_path = os.path.join(disk_real_full_images_path, image_name)

        if not image_name.split('.')[-1] in SUFFIXES:
            continue

        image = Image.open(image_path)
        image = np.asarray(image)
        rows, cols, _ = image.shape

        rows_lines = ceil(rows / side_size)
        cols_lines = ceil(cols / side_size)

        for rl in range(rows_lines):
            for cl in range(cols_lines):
                image_rl_cl = image[rl * side_size: (rl + 1) * side_size, cl * side_size: (cl + 1) * side_size]
                image_rl_cl_path = os.path.join(disk_new_full_images_path, 
                                                image_name.split('.')[0] + f'#{rl}#{cl}#.' + image_name.split('.')[-1])
                Image.fromarray(image_rl_cl, mode='RGB').save(image_rl_cl_path)