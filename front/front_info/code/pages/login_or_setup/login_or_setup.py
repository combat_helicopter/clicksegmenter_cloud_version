from dash import ctx, dcc


def redirect_from_start_(btn1,  btn2):
    if ctx.triggered_id == 'start-login-button':
        return dcc.Location(pathname='/login', id='start-login-redirect')
    if ctx.triggered_id == 'start-signup-button':
        return dcc.Location(pathname='/signup', id='start-signup-redirect')
