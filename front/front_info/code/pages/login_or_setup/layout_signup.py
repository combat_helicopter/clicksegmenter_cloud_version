from dash import Input, State, Output, html, dcc

from pages.main import app
from pages.login_or_setup.signup import signup_

signup_layout = html.Div(
    [
        html.Div(
            style={'width': 100, 'height': 200}
        ), 
        html.Div(
            children='SignUp',
            style={'textAlign': 'center'}
        ),
        html.Div(
            dcc.Input(
                id='signup-input',
                style={'margin': 18, 'width': 200, 'height': 30}
            ), 
            style={'textAlign': 'center'}
        ),
        html.Div(
            id='signup-redirect',
            style={'textAlign': 'center', 'height': 30}
            ), 
        html.Div(
            html.Button(
                'SEND', 
                id='signup-send-button'
            ), 
            style={'textAlign': 'center'}
        )
    ]
)

@app.callback(
    Output('signup-redirect', 'children'),
    State('signup-input', 'value'),
    Input('signup-send-button', 'n_clicks')
)
def signup(input, btn):
    return signup_(input, btn)