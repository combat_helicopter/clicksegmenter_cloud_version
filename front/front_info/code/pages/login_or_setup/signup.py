import os
from dash import html, ctx, dcc

from pages.main import build_user_class_path, CLASSES_NUMBER
from db_tools.tools import add_user_db, nickname_already_exists_db, uoi_insert_row_db


def signup_(input, btn):
    global CLASSES_NUMBER
    if ctx.triggered_id == 'signup-send-button':
        if not nickname_already_exists_db(input):
            add_user_db(input)
            uoi_insert_row_db()

            for c in range(CLASSES_NUMBER):
                for v in build_user_class_path(input, c).values():
                    os.makedirs(v, 0o777, exist_ok=True)

            return dcc.Location(pathname='/login', id='signup-redirect')
        else:
            return html.Div(id='signup-redirect', children='login already exists')

