import dash
from dash import ctx, dcc, html

from db_tools.tools import nickname_already_exists_db


def login_(input, btn):
    if ctx.triggered_id == 'login-send-button':
        if nickname_already_exists_db(input):
            dash.callback_context.response.set_cookie('segment_login', input)
            return dcc.Location(pathname='/images_view', id='login-redirect')
        else:
            return html.Div(id='login-redirect', children='incorrect login')