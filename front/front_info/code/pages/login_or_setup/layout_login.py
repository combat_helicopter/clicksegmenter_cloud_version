from dash import Input, State, Output, html, dcc

from pages.main import app
from pages.login_or_setup.login import login_


login_layout = html.Div(
    [
        html.Div(
            style={'width': 100, 'height': 200}
        ), 
        html.Div(
            children='LogIn', 
            style={'textAlign': 'center'}
        ),
        html.Div(
            dcc.Input(
                id='login-input', 
                style={'margin': 18, 'width': 200, 'height': 30}
            ), 
            style={'textAlign': 'center'}
        ), 
        html.Div(
            id='login-redirect',
            style={'textAlign': 'center', 'height': 30}
            ), 
        html.Div(
            html.Button(
                children='SEND', 
                id='login-send-button',
                style={'margine':10}
            ),
            style={'textAlign': 'center'}
        )
    ]
)


@app.callback(
    Output('login-redirect', 'children'),
    State('login-input', 'value'),
    Input('login-send-button', 'n_clicks')
)
def login(input, btn):
    return login_(input, btn)