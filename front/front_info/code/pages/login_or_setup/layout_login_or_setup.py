from dash import Input, Output, html

from pages.main import app
from pages.login_or_setup.login_or_setup import redirect_from_start_


start_layout = html.Div(
    [
        html.Div(
            style={'width': 100, 'height': 200}
        ), 
        html.Div(
            children='Hi, friend', 
            style={'textAlign': 'center', 'margin': 9}
        ),
        html.Div(
            html.Button(
                children='Log In', 
                id='start-login-button', 
                style={'textAlign': 'center', 'margin': 9, 'width': 200, 'height': 30}
            ), 
            style={'textAlign': 'center'}
        ),
        html.Div(
            html.Button(
                children='Sign Up', 
                id='start-signup-button', 
                style={'textAlign': 'center', 'margin': 9, 'width': 200, 'height': 30}
            ), 
            style={'textAlign': 'center'}
        ),
        html.Div(id='start-redirect')
    ]
)


@app.callback(
    Output('start-redirect', 'children'),
    Input('start-login-button', 'n_clicks'),
    Input('start-signup-button', 'n_clicks')
)
def redirect_from_start(btn1,  btn2):
    return redirect_from_start_(btn1,  btn2)