import os
from math import ceil
from collections import defaultdict

import flask
import plotly.express as px
import dash_bootstrap_components as dbc
from dash import Input, State, Output, html, ctx, dcc, no_update

import numpy as np
from PIL import Image

import torch
from torchvision.io import read_image
from torchvision.utils import save_image
from torchvision.transforms import Resize, ToTensor
from torchvision.transforms.functional import to_pil_image, InterpolationMode


from pages.main import (
    app, 
    DISK_FULL_IMAGES_PATH, DISK_FULL_IMAGES_VIEW_PATH, DISK_CROPS_PATH, MASKS_COLORS,
    CLASSES_NUMBER, GRAPH_SIZE, PATCH_SIZE, VIEW_SIZE, CLASSES_MAP, INVERSE_CLASSES_MAP, SUFFIXES,
    build_user_class_path
    )
from db_tools.tools import (
    get_user_info_db, append_image_db, get_image_name_db_user, 
    change_current_image_number_db_user, change_current_crop_number_db_user, 
    nickname_already_exists_db, 
    get_db_size_db, get_uoi_db, uoi_delete_image_db, uoi_add_image_db, 
    get_ip_db, change_current_image_parameters_ip_db
    )


def get_image_size(image_name):
    make_crops(image_name)
    return get_ip_db(image_name)

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def get_nickname():
    cookies = dict(flask.request.cookies)
    if 'segment_login' not in cookies:
        return None
    
    nickname = cookies['segment_login']
    if not nickname_already_exists_db(nickname):
        return None

    return nickname

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def add_new_image_to_user(nickname, user_image_names):
    USERS_ON_IMAGE_DICT = get_uoi_db()
    for users_number in sorted(USERS_ON_IMAGE_DICT):
        pretenders = USERS_ON_IMAGE_DICT[users_number] - set(user_image_names)
        if 0 == len(pretenders):
            continue
        new_image_name = np.random.choice(list(pretenders))
        
        uoi_delete_image_db(users_number, new_image_name)
        uoi_add_image_db(users_number + 1, new_image_name)
        break

    append_image_db(nickname, new_image_name)
    change_current_image_number_db_user(nickname, len(user_image_names))
    return new_image_name


def get_image_name(nickname, next=False, prev=False):
    user_info = get_user_info_db(nickname)

    if -1 == user_info.last_image_number:
        image_name = add_new_image_to_user(nickname, user_info.images_order)
        user_info = get_user_info_db(nickname)

    image_name = user_info.images_order[user_info.last_image_number]

    if next:
        if user_info.last_image_number < len(user_info.images_order) - 1:
            change_current_image_number_db_user(nickname, user_info.last_image_number + 1)
            image_name = get_image_name_db_user(nickname, user_info.last_image_number + 1)
        else:
            if len(user_info.images_order) < get_db_size_db('images_parameters'):
                image_name = add_new_image_to_user(nickname, user_info.images_order)

    if prev:
        if user_info.last_image_number > 0:
            change_current_image_number_db_user(nickname, user_info.last_image_number - 1)
            image_name = get_image_name_db_user(nickname, user_info.last_image_number - 1)
        else:
            image_name = user_info.images_order[0]
    
    for c in range(CLASSES_NUMBER):
        for v in build_user_class_path(nickname, c).values():
            pth = os.path.join(v, image_name.split('.')[0])
            if not os.path.exists(pth):
                os.makedirs(pth, 0o774, exist_ok=True)
    return image_name

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def make_crops(image_name):
    global DISK_FULL_IMAGES_PATH, DISK_CROPS_PATH

    if get_ip_db(image_name)[0] is not None:
        return

    image = Image.open(os.path.join(DISK_FULL_IMAGES_PATH, image_name)).convert('RGB')
    image_np = np.asarray(image)
    rows, cols = image_np.shape[:2]

    os.makedirs(os.path.join(DISK_CROPS_PATH, image_name.split('.')[0]), 0o774, exist_ok=True)

    rows = range(0, rows, PATCH_SIZE)
    cols = range(0, cols, PATCH_SIZE)
    for ir, row in enumerate(rows):
        for ic, col in enumerate(cols):
            image_crop_name = image_name.split('.')[0] + f'({ir},{ic}).jpeg'
            image_crop_path = os.path.join(DISK_CROPS_PATH, image_name.split('.')[0], image_crop_name)
            if not os.path.exists(image_crop_path):
                image_crop_np = image_np[row: row+PATCH_SIZE, col: col+PATCH_SIZE]
                image_crop = Image.fromarray(image_crop_np)
                image_crop.save(image_crop_path)
    
    change_current_image_parameters_ip_db(image_name, len(rows), len(cols), *image_np.shape[:2])

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def merge_crops(nickname, image_name, class_type, image_view_shape):
    global SUFFIXES, VIEW_SIZE, PATCH_SIZE

    path = os.path.join(build_user_class_path(nickname, class_type)['disk_masks_view'], image_name.split('.')[0])
    mask_crops_names = [
        mask_crop_name for mask_crop_name in 
        os.listdir(path) if mask_crop_name.split('.')[-1] in SUFFIXES]
    
    mask_crops_info = [(m, *map(int, m.split('(')[1].split(')')[0].split(','))) for m in mask_crops_names]
    
    _, _, image_rows, _ = get_image_size(image_name)
    image_view_rows = image_view_shape[-2]
    scale = min(image_view_rows / image_rows, 1)
    patch_view_size = ceil(PATCH_SIZE * scale)
    
    full_image = torch.zeros(*image_view_shape, dtype=torch.uint8)
    for mask_crop_name, row, col in mask_crops_info:
        patch = read_image(os.path.join(path, mask_crop_name))[0].type(torch.uint8)

        row_from = int(row * PATCH_SIZE * scale)
        col_from = int(col * PATCH_SIZE * scale)
        row_to = row_from + patch_view_size
        col_to = col_from + patch_view_size

        if row_to > full_image.shape[0]:
            row_from = -patch.shape[0]
            row_to = full_image.shape[0]
        if col_to > full_image.shape[1]:
            col_from = -patch.shape[1]
            col_to = full_image.shape[1]
        
        full_image[row_from: row_to, col_from: col_to] = patch

    return full_image


def resize(image, rows, make_grid=False):
    global PATCH_SIZE

    scale = min(rows / image.shape[-2], 1)
    size = (int(image.shape[-2] * scale), int(image.shape[-1] * scale))

    if make_grid:
        image[:, ::PATCH_SIZE] = 255
        image[:, 1::PATCH_SIZE] = 255
        image[:, :, ::PATCH_SIZE] = 255
        image[:, :, 1::PATCH_SIZE] = 255

        image = to_pil_image(image, mode='RGB')
        image = image.resize(size[::-1])
    else:
        image = Resize(size=size, interpolation=InterpolationMode.NEAREST)(image)

    return image


def view(image_name, rows, nickname=None):
    global DISK_FULL_IMAGES_PATH, DISK_FULL_IMAGES_VIEW_PATH, MASKS_COLORS, INVERSE_CLASSES_MAP, CLASSES_MAP

    is_exist = os.path.exists(os.path.join(DISK_FULL_IMAGES_VIEW_PATH, image_name))
    if not is_exist:
        image = read_image(os.path.join(DISK_FULL_IMAGES_PATH, image_name))
        image = resize(image, rows, make_grid=True)
        image.save(os.path.join(DISK_FULL_IMAGES_VIEW_PATH, image_name))
        return image

    
    image = read_image(os.path.join(DISK_FULL_IMAGES_VIEW_PATH, image_name))
    if nickname is not None:
        class_types = [ct for ct in CLASSES_MAP.keys()]
        class_numbers = [CLASSES_MAP[ct] for ct in class_types]

        masks = [
            merge_crops(nickname, image_name, cn, image.shape[-2:]) / 255
            for cn in class_numbers
        ]

        image = image.type(torch.float32)
        for class_type, mask in zip(class_types, masks):
            alpha = 0.5
            color = torch.tensor(MASKS_COLORS[class_type])
            image = (1 - alpha * mask[None]) * image + alpha * mask[None] * color[:, None, None] * 255
        
        image = image.type(torch.uint8)

    image = to_pil_image(image, mode='RGB')
    return image


######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def display_(btn1, btn2, btn3, btn4, fig1):
    global DISK_FULL_IMAGES_PATH, VIEW_SIZE

    nickname = get_nickname()
    if nickname is None:
        return dcc.Location(pathname='/', id='images-view-redirect-1'), no_update

    image_name = get_image_name(nickname)

    is_prev = (ctx.triggered_id == 'image-prev')
    is_next = (ctx.triggered_id == 'image-next')
    is_show_image = (ctx.triggered_id == 'image-show-image')
    is_show_markup = (ctx.triggered_id == 'image-show-markup')

    if is_show_image:
        image = view(image_name, rows=VIEW_SIZE)
    else:
        if is_prev or is_next:
            image_name = get_image_name(nickname, prev=is_prev, next=is_next)
        image = view(image_name, rows=VIEW_SIZE, nickname=nickname)
    fig = px.imshow(np.array(image), height=GRAPH_SIZE)
    return no_update, fig


def choose_crop_(click_position):
    global VIEW_SIZE, PATCH_SIZE

    cookies = dict(flask.request.cookies)
    nickname = cookies['segment_login']

    user_info = get_user_info_db(nickname)
    image_name = get_image_name(nickname)

    row_crops, col_crops, rows, cols = get_image_size(image_name)
    scale = min(VIEW_SIZE / rows, 1)

    row = int(click_position['points'][0]['y'] / scale) // PATCH_SIZE
    col = int(click_position['points'][0]['x'] / scale) // PATCH_SIZE

    crop_number = row * col_crops + col
    change_current_crop_number_db_user(nickname, 
                                       user_info.last_image_number, 
                                       crop_number)

    return dcc.Location(pathname='/markup', id='images-view-redirect-3')