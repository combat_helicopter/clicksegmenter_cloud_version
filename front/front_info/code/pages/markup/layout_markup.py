import os

import numpy as np
import plotly.express as px
import dash_bootstrap_components as dbc
from dash import Input, State, Output, html, dcc

from pages.main import app, CLASSES_MAP, GRAPH_SIZE
from pages.markup.markup import (display_, update_crop_, choose_click_type_, back_to_views_, get_class_, 
                                 white_button_style, green_button_style, red_button_style)


height=GRAPH_SIZE
default_image = np.zeros(shape=(1, 1, 3)) + 255
fig = px.imshow(default_image, height=height)
fig.update_layout(coloraxis_showscale=False)
fig.update_xaxes(showticklabels=False)
fig.update_yaxes(showticklabels=False)


markup_layout = html.Div([
    html.Div(
        [
            dcc.Graph(figure=fig, id='crop-graph'),
        ],
        style={'width': '80%', 'display': 'inline-block'}
    ),

    html.Div(
        [
            html.Div(
                [
                    dcc.RadioItems(
                        list(CLASSES_MAP),
                        list(CLASSES_MAP)[0],
                        id='crop-class',
                        style={'textAlign': 'left', 'margin': 9}
                    )
                ],
                style={'hight': '50%', 'verticalAlign': 'top', 'textAlign': 'center'}
            ),
            html.Div(
                [
                    dbc.Col(
                        [
                            dbc.Row(html.Div([
                                html.Button(children='POSITIVE', id='crop-positive', n_clicks=0,
                                style=green_button_style)
                            ],style={'textAlign': 'center'})),
                            dbc.Row(html.Div([
                                html.Button(children='NEGATIVE', id='crop-negative', n_clicks=0,
                                style=white_button_style)
                            ], style={'textAlign': 'center'})),


                            html.Div([
                                dbc.Row(
                                    html.Button(children='UP', id='crop-up', n_clicks=0, 
                                        style={'margin': 0, 'width': 50, 'height': 50}), 
                                    justify='center'
                                ),
                                dbc.Row([
                                    html.Button(children='PREV', id='crop-prev', n_clicks=0,
                                        style={'margin': 0, 'width': 50, 'height': 50}), 
                                    html.Button(children='o',
                                        style={'margin': 0, 'width': 50, 'height': 50}), 
                                    html.Button(children='NEXT', id='crop-next', n_clicks=0,
                                        style={'margin': 0, 'width': 50, 'height': 50})
                                    ], justify='center', style={'margin': 0, 'width': 150, 'height': 50}
                                ),
                                dbc.Row(
                                    html.Button(children='DOWN', id='crop-down', n_clicks=0,
                                        style={'margin': 0, 'width': 50, 'height': 50}), 
                                    justify='center'
                                )
                            ], style={'margin': 25, 'width': 150, 'height': 150}), 


                            dbc.Row(html.Div([
                                html.Button(children='RESET', id='crop-reset', n_clicks=0,
                                style={'margin': 9, 'width': 200, 'height': 30})
                                ], style={'textAlign': 'center'}
                            )),
                            dbc.Row(html.Div([
                                html.Button(children='BACK TO IMAGES', id='crop-back', n_clicks=0,
                                style={'margin': 9, 'width': 200, 'height': 30})
                                ], style={'textAlign': 'center'}
                            ))
                        ],
                    )
                ],  
                style={'hight': '50%', 'verticalAlign': 'bottom'}
            )
        ],
        style={'width': '20%', 'display': 'inline-block', 'textAlign':'center'}
    ),

    html.Div(id='crops-redirect')
])


#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
@app.callback(
    Output('crop-graph', 'figure', allow_duplicate=True),
    Output('crops-redirect', 'children', allow_duplicate=True),

    Input('crop-class', 'value'),

    Input('crop-prev', 'n_clicks'),
    Input('crop-next', 'n_clicks'),
    Input('crop-up', 'n_clicks'),
    Input('crop-down', 'n_clicks'),
    
    Input('crop-reset', 'n_clicks'),
    
    State('crop-graph', 'figure'),

    prevent_initial_call='initial_duplicate'
)
def display(rd1, btn1, btn2, btn3, btn4, btn5, fig1):
    return display_(rd1, btn1, btn2, btn3, btn4, btn5, fig1)


@app.callback(
    Output('crop-graph', 'figure'),
    Input('crop-graph', 'clickData'),

    prevent_initial_call=True
)
def update_crop(click_position):
    return update_crop_(click_position)


@app.callback(
    Output('crop-positive', 'style'), 
    Output('crop-negative', 'style'),
    Input('crop-positive', 'n_clicks'),
    Input('crop-negative', 'n_clicks')
)
def choose_click_type(btn1, btn2):
    return choose_click_type_(btn1, btn2)


@app.callback(
    Output('crops-redirect', 'children'),
    Input('crop-back', 'n_clicks'), 

    prevent_initial_call=True
)
def back_to_views(btn):
    return back_to_views_(btn)


@app.callback(
    Output('crop-class', 'value'),
    Input('crop-class', 'value')
)
def get_class(rd):
    return get_class_(rd)
