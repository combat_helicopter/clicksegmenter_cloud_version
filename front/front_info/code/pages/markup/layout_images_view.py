import os

import numpy as np
import plotly.express as px
import dash_bootstrap_components as dbc
from dash import Input, State, Output, html, dcc

from pages.main import app, GRAPH_SIZE
from pages.markup.images_view import display_, choose_crop_


height = GRAPH_SIZE
default_image = np.zeros(shape=(1, 1, 3)) + 255
fig = px.imshow(default_image, height=height)
fig.update_layout(coloraxis_showscale=False)
fig.update_xaxes(showticklabels=False)
fig.update_yaxes(showticklabels=False)


images_view_layout = html.Div([
    html.Div(
        [
            dcc.Graph(figure=fig, id='image-graph'),
        ],
        style={'width': '80%', 'display': 'inline-block'}
    ),

    html.Div(
        [
            dbc.Col(
                [
                    dbc.Row(html.Div(
                        [
                            html.Button(
                                children='SHOW MARKUP', id='image-show-markup', n_clicks=0,
                                style={'margin': 9, 'width': 200, 'height': 30}
                                )
                        ], 
                        style={'textAlign': 'center'}
                    )),
                    dbc.Row(html.Div(
                        [
                            html.Button(
                                children='SHOW IMAGE', id='image-show-image', n_clicks=0,
                                style={'margin': 9, 'width': 200, 'height': 30}
                                )
                        ], 
                        style={'textAlign': 'center'}
                    )),
                    dbc.Row(html.Div(
                        [
                            html.Button(
                                children='PREV', id='image-prev', n_clicks=0, 
                                style={'margin': 9, 'width': 200, 'height': 30}
                                )
                        ], 
                        style={'textAlign': 'center'}
                    )),
                    dbc.Row(html.Div(
                        [
                            html.Button(
                                children='NEXT', id='image-next', n_clicks=0, 
                                style={'margin': 9, 'width': 200, 'height': 30}
                                )
                        ], 
                        style={'textAlign': 'center'}
                    ))
                ],
            ), 
            html.Div(
                style={'width': 100, 'height': 100}
            ), 
        ],
        style={'width': '20%', 'display': 'inline-block', 'textAlign':'center', 'verticalAlign': 'bottom'}
    ),
    html.Div(id='images-view-redirect')
])


@app.callback(
    Output('images-view-redirect', 'children', allow_duplicate=True),
    Output('image-graph', 'figure'),
    
    Input('image-show-markup', 'n_clicks'),
    Input('image-show-image', 'n_clicks'),
    Input('image-prev', 'n_clicks'),
    Input('image-next', 'n_clicks'),
    
    State('image-graph', 'figure'), 

    prevent_initial_call='initial_duplicate'
)
def display(btn1, btn2, btn3, btn4, fig1):
    return display_(btn1, btn2, btn3, btn4, fig1)


@app.callback(
    Output('images-view-redirect', 'children'),
    Input('image-graph', 'clickData'),

    prevent_initial_call=True
)
def choose_crop(click_position):
    return choose_crop_(click_position)