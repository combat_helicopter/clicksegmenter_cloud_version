import os
from math import ceil

import torch
import numpy as np
import plotly.express as px
import tritonclient.http as httpclient
from dash import ctx, dcc, no_update
from PIL import Image
from torchvision.transforms import ToTensor

from pages.markup.images_view import make_crops, get_nickname
from pages.main import (DISK_CROPS_PATH, POINTS_NUMBER_FOR_CLASS, HEADERS, MODEL_ID, 
                        POSITIVE_CLICK, NEGATIVE_CLICK, CLASSES_MAP, INVERSE_CLASSES_MAP, 
                        GRAPH_SIZE, VIEW_SIZE, PATCH_SIZE, MASKS_COLORS,
                        build_user_class_path)
from db_tools.tools import (get_user_info_db, 
                            change_current_crop_number_db_user, 
                            change_current_click_type_db_user, 
                            change_current_class_type_db_user, 
                            get_ip_db)
from model_tools.dist_maps_tool import DistMaps

def get_crop_name(user_info, next=False, prev=False):
    image_name = user_info.images_order[user_info.last_image_number]
    make_crops(image_name)
    crop_number = user_info.last_crops_number[user_info.last_image_number]
    cc = get_ip_db(image_name)[1]
    ir = crop_number // cc
    ic = crop_number % cc
    crop_name = image_name.split('.')[0] + f'({ir},{ic}).jpeg'
    return crop_name

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def get_points(nickname, crop_name, class_type):
    global POINTS_NUMBER_FOR_CLASS
    crop_path = os.path.join(
        build_user_class_path(nickname, class_type)['disk_points'], 
        crop_name.split('(')[0], 
        crop_name.split('.')[0] + '.npy'
        )
    
    if os.path.exists(crop_path):
        points = np.load(crop_path)
    else:
        points = np.full((POINTS_NUMBER_FOR_CLASS, 3), -1)
    
    return points

def updata_points(points, new_point, is_positive=True):
    global POINTS_NUMBER_FOR_CLASS

    up_points = points[:POINTS_NUMBER_FOR_CLASS//2] if is_positive else points[POINTS_NUMBER_FOR_CLASS//2:]
    up_mask = np.all((up_points != -1), axis=1)
    up_points = up_points[up_mask]

    if 0 == len(up_points):
        up_points = np.array([new_point])
    else:
        up_points = np.concatenate([up_points, np.array([new_point])], axis=0)[-POINTS_NUMBER_FOR_CLASS//2:]

    if is_positive:
        points[:len(up_points)] = up_points
    else:
        points[POINTS_NUMBER_FOR_CLASS//2:POINTS_NUMBER_FOR_CLASS//2 + len(up_points)] = up_points

    return points

def save_points(points, nickname, crop_name, class_type):
    global POINTS_NUMBER_FOR_CLASS
    crop_path = os.path.join(
        build_user_class_path(nickname, class_type)['disk_points'], 
        crop_name.split('(')[0], 
        crop_name.split('.')[0] + '.npy'
        )
    np.save(crop_path, points)

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def get_mask(nickname, crop_name, class_type, shape):
    crop_path = os.path.join(
        build_user_class_path(nickname, class_type)['disk_masks'], 
        crop_name.split('(')[0], 
        crop_name.split('.')[0] + '.png'
        )
    
    if os.path.exists(crop_path):
        mask = Image.open(crop_path)
    else:
        mask = np.zeros(shape=shape, dtype=np.uint8)
        mask = Image.fromarray(mask, mode='L')
    
    return mask

def save_mask(mask, nickname, crop_name, class_type):
    crop_path = os.path.join(
        build_user_class_path(nickname, class_type)['disk_masks'], 
        crop_name.split('(')[0], 
        crop_name.split('.')[0] + '.png'
        )
    mask.save(crop_path)

def save_mask_view(mask, nickname, crop_name, class_type, image_rows, image_view_rows):
    crop_path = os.path.join(
        build_user_class_path(nickname, class_type)['disk_masks_view'], 
        crop_name.split('(')[0], 
        crop_name.split('.')[0] + '.png'
        )

    scale = min(image_view_rows / image_rows, 1)
    newsize = (ceil(mask.size[-2] * scale), ceil(mask.size[-1] * scale))

    mask_view = mask.resize(newsize)
    mask_view.save(crop_path)

def sum_image_mask_and_points(mask_batch, image_batch, coord_features, class_type):
    global INVERSE_CLASSES_MAP, MASKS_COLORS
    alpha_low = 0.5
    
    color = torch.tensor(MASKS_COLORS[INVERSE_CLASSES_MAP[class_type]])[None, :, None, None]
    show = torch.clone(image_batch)
    show = torch.where(mask_batch[0] > 0.2, 
                       (1 - alpha_low) * show + alpha_low * (color * mask_batch[0]), 
                       show)

    show[0][0][coord_features[0, 1]] = 0.1
    show[0][1][coord_features[0, 0]] = 1.
    show[0][2][coord_features[0, 0]] = 0.1

    show[0][0][coord_features[0, 1]] = 1.
    show[0][1][coord_features[0, 1]] = 0.1
    show[0][2][coord_features[0, 1]] = 0.1

    show = show[0].numpy().transpose(1, 2, 0)

    return show
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
def call_model(crop, mask, points):
    global HEADERS, MODEL_ID, PATCH_SIZE

    triton_client = httpclient.InferenceServerClient(url="node-api.datasphere.yandexcloud.net", ssl=True)

    crop = np.asarray(crop).transpose(2, 0, 1)
    mask = np.asarray(mask)

    crop_mask_input = np.concatenate([crop, mask[None]], axis=0)[None]
    if (crop.shape[-1] < PATCH_SIZE) or (crop.shape[-2] < PATCH_SIZE):
        zero_crop_mask = np.zeros(shape=(*crop_mask_input.shape[:2], PATCH_SIZE, PATCH_SIZE), dtype=np.uint8)
        zero_crop_mask[:, :, :crop.shape[-2], :crop.shape[-1]] = crop_mask_input
        crop_mask_input = zero_crop_mask
    points_input = points.astype(np.float32)[None]

    crop_mask_payload = httpclient.InferInput("input_variable_0", crop_mask_input.shape, "UINT8")
    points_payload = httpclient.InferInput("input_variable_1", points_input.shape, "FP32")

    crop_mask_payload.set_data_from_numpy(crop_mask_input, binary_data=False)
    points_payload.set_data_from_numpy(points_input, binary_data=False)

    inputs = [crop_mask_payload, points_payload]
    results=triton_client.infer(MODEL_ID, inputs=inputs, headers=HEADERS)

    new_mask = results.as_numpy("output_variable_0")[:, :, :crop.shape[-2], :crop.shape[-1]]
    new_mask = torch.tensor(new_mask, dtype=torch.float32) / 255

    # new_mask = torch.tensor(np.random.rand(*(mask[None, None].shape)))
    return new_mask

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
DIST_MAPS = DistMaps(
    norm_radius=3,
    spatial_scale=1.0,
    use_disks=True,
)

white_button_style = {'margin': 9, 
                      'width': 200, 
                      'height': 30}

green_button_style = {'background-color': 'green',
                      'color': 'white',
                      'margin': 9, 
                      'width': 200, 
                      'height': 30}

red_button_style = {'background-color': 'red',
                    'color': 'white',
                    'margin': 9, 
                    'width': 200, 
                    'height': 30}

def display_(rd1, btn1, btn2, btn3, btn4, btn5, fig1):
    global DISK_CROPS_PATH, DIST_MAPS, POINTS_NUMBER_FOR_CLASS, CLASSES_MAP, VIEW_SIZE

    nickname = get_nickname()
    if nickname is None:
        return no_update, dcc.Location(pathname='/', id='crops-redirect-1')

    if ctx.triggered_id == 'crop-class':
        change_current_class_type_db_user(nickname, CLASSES_MAP[rd1])

    user_info = get_user_info_db(nickname)
    image_name = user_info.images_order[user_info.last_image_number]
    crop_name = get_crop_name(user_info)
    crop_number = user_info.last_crops_number[user_info.last_image_number]

    if ctx.triggered_id == 'crop-prev':
        change_current_crop_number_db_user(nickname, 
                                           user_info.last_image_number, 
                                           max(0, crop_number - 1))
    if ctx.triggered_id == 'crop-next':
        cr, cc, _, _ = get_ip_db(image_name)
        last_crop_number = cr * cc - 1
        change_current_crop_number_db_user(nickname, 
                                           user_info.last_image_number, 
                                           min(crop_number + 1, last_crop_number))
    if ctx.triggered_id == 'crop-up':
        cr, cc, _, _ = get_ip_db(image_name)
        last_crop_number = cr * cc - 1
        change_current_crop_number_db_user(nickname, 
                                           user_info.last_image_number, 
                                           max(0, crop_number - cc))
    if ctx.triggered_id == 'crop-down':
        cr, cc, _, _ = get_ip_db(image_name)
        last_crop_number = cr * cc - 1
        change_current_crop_number_db_user(nickname, 
                                           user_info.last_image_number, 
                                           min(last_crop_number, crop_number + cc))

    user_info = get_user_info_db(nickname)
    crop_name = get_crop_name(user_info)
    image_name = user_info.images_order[user_info.last_image_number]
    _, _, rows, _ = get_ip_db(image_name)

    crop = Image.open(os.path.join(DISK_CROPS_PATH, image_name.split('.')[0], crop_name))
    mask = get_mask(nickname, crop_name, user_info.class_type, crop.size[::-1])
    points = get_points(nickname, crop_name, user_info.class_type)

    if ctx.triggered_id == 'crop-reset':
        points = np.full((POINTS_NUMBER_FOR_CLASS, 3), -1)
        mask = Image.fromarray(np.asarray(mask, dtype=np.uint8) * 0, mode='L')
        save_points(points, nickname, crop_name, user_info.class_type)
        save_mask(mask, nickname, crop_name, user_info.class_type)
        save_mask_view(mask, nickname, crop_name, user_info.class_type, rows, VIEW_SIZE)

    mask_batch = ToTensor()(mask)[None, None]
    crop_batch = ToTensor()(crop)[None]
    points_batch = torch.tensor(points[None])
    coord_features = DIST_MAPS(crop_batch, points_batch).bool()

    show = sum_image_mask_and_points(mask_batch, crop_batch, coord_features, user_info.class_type)
    fig = px.imshow(show, height=GRAPH_SIZE)
    return fig, no_update


def update_crop_(click_position):
    global DISK_CROPS_PATH, DIST_MAPS, VIEW_SIZE

    new_point = [click_position['points'][0]['y'], click_position['points'][0]['x'], 100]

    nickname = get_nickname()
    if nickname is None:
        return no_update

    user_info = get_user_info_db(nickname)
    image_name = user_info.images_order[user_info.last_image_number]
    crop_name = get_crop_name(user_info)
    _, _, rows, _ = get_ip_db(image_name)

    crop = Image.open(os.path.join(DISK_CROPS_PATH, image_name.split('.')[0], crop_name))
    mask = get_mask(nickname, crop_name, user_info.class_type, crop.size[::-1])
    points = get_points(nickname, crop_name, user_info.class_type)
    
    if user_info.click_type:
        points = updata_points(points, new_point, is_positive=True)
    else:
        points = updata_points(points, new_point, is_positive=False)
    save_points(points, nickname, crop_name, user_info.class_type)

    mask_batch = call_model(crop, mask, points)
    crop_batch = ToTensor()(crop)[None]
    points_batch = torch.tensor(points[None])
    coord_features = DIST_MAPS(crop_batch, points_batch).bool()

    mask = Image.fromarray((mask_batch[0, 0] * 255).numpy().astype(np.uint8), mode='L')
    save_mask(mask, nickname, crop_name, user_info.class_type)
    save_mask_view(mask, nickname, crop_name, user_info.class_type, rows, VIEW_SIZE)

    show = sum_image_mask_and_points(mask_batch, crop_batch, coord_features, user_info.class_type)
    fig = px.imshow(show, height=GRAPH_SIZE)

    return fig


def choose_click_type_(btn1, btn2):
    global POSITIVE_CLICK, NEGATIVE_CLICK

    nickname = get_nickname()
    if nickname is None:
        return no_update, no_update

    if ctx.triggered_id == 'crop-negative':
        change_current_click_type_db_user(nickname, NEGATIVE_CLICK)
        return white_button_style, red_button_style
    else:
        change_current_click_type_db_user(nickname, POSITIVE_CLICK)
        return green_button_style, white_button_style


def back_to_views_(btn):
    return dcc.Location(pathname='/images_view', id='crops-redirect-2')


def get_class_(rd):
    nickname = get_nickname()
    if nickname is None:
        return no_update

    return rd
