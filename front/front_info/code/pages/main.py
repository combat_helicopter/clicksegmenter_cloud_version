import os
import sys

import flask
import numpy as np
from dash import Dash

from shutil import rmtree
from pages.preprocessing.cut_photos import cut_photos
from config import CLASSES, SIDE_SIZE, RESET_ALL

DISKS_PATH = os.path.join(os.environ['DISKS'], 'disks')
DISK_FULL_IMAGES_PATH = os.path.join(os.environ['DISKS'], 'disk_full_images')
DISK_FULL_IMAGES_VIEW_PATH = os.path.join(os.environ['DISKS'], 'disk_full_images_view')
DISK_CROPS_PATH = os.path.join(os.environ['DISKS'], 'disk_crops')
REAL_FULL_IMAGES_PATH = os.environ['REAL_FULL_IMAGES_PATH']

PERSONAL_DISK_NAMES = ['disk_masks', 'disk_points', 'disk_masks_view']

# CLASSES_MAP = {
#     'Железо': 0, 
#     'Рыболовные снасти': 1, 
#     'Пластик': 2, 
#     'Дерево': 3, 
#     'Бетон': 4, 
#     'Резина': 5
#     }

# MASKS_COLORS = {
#         'Железо' : [1., 0.4, 0.], 
#         'Рыболовные снасти' : [0., 1., 1.], 
#         'Пластик' : [0., 1., 0.], 
#         'Дерево' : [1., 1., 0.], 
#         'Бетон' : [1., 0., 0.], 
#         'Резина' : [1., 0.2, 1.]
#     }


R_STEP = 0.3
G_STEP = 0.5
B_STEP = 0.7
INTENSITY = 2

CLASSES_MAP = {c: i for i, c in enumerate(CLASSES)}
MASKS_COLORS = {}
for i, c in enumerate(CLASSES):
    a = np.array([((i + 1) * R_STEP)%1.3, ((i + 1) * G_STEP)%1.3, ((i + 1) * B_STEP)%1.3])
    a = a / a.max()
    MASKS_COLORS[c] = a

for k, v in CLASSES_MAP.items():
    print(f'{k}: class_{v}')

INVERSE_CLASSES_MAP = {
    v: k for k, v in CLASSES_MAP.items()
}

CLASSES_NUMBER = len(CLASSES_MAP)
POINTS_NUMBER_FOR_CLASS = 100
PATCH_SIZE = int(os.environ['PATCH_SIZE'])
VIEW_SIZE = 350
GRAPH_SIZE = 700
POSITIVE_CLICK = True
NEGATIVE_CLICK = False

TOKEN = os.environ['TOKEN']
NODE_ID = os.environ['NODE_ID']
FOLDER_ID = os.environ['FOLDER_ID']
MODEL_ID = os.environ['MODEL_ID']
HEADERS = {
    "Authorization": f"Api-key {TOKEN}",
    "x-node-id": f"{NODE_ID}",
    "x-folder-id": f"{FOLDER_ID}"
}

print('Запоминаю имена...', file=sys.stderr)
SUFFIXES = ['jpg', 'jpeg', 'png', 'JPG', 'JPEG']
IMAGE_NAMES = [
    imgn for imgn in os.listdir(DISK_FULL_IMAGES_PATH) 
    if imgn.split('.')[-1] in SUFFIXES
]


def build_user_class_path(nickname, cls):
    global DISKS_PATH, PERSONAL_DISK_NAMES
    pths = {
        pdn : os.path.join(DISKS_PATH, nickname, pdn, f'class_{cls}')
        for pdn in PERSONAL_DISK_NAMES
    }
    return pths

print('Запускаю сервер...', file=sys.stderr)
server = flask.Flask(__name__)
app = Dash(__name__, use_pages=True, pages_folder='', server=server)

print('main done!', file=sys.stderr)
