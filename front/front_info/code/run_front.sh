#!/bin/bash

python3 setup.py
gunicorn app:server -b 0.0.0.0:7004 -w 5 --threads 5