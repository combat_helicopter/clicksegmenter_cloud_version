import os
import sys

import dash
# from async_dash import Dash
# from dash import Dash
from dash import html
# from sqlalchemy import create_engine

from config import RESET_ALL

from pages.main import app, server, DISK_FULL_IMAGES_PATH, SUFFIXES, IMAGE_NAMES

from pages.login_or_setup.layout_login_or_setup import start_layout
from pages.login_or_setup.layout_login import login_layout
from pages.login_or_setup.layout_signup import signup_layout

from pages.markup.layout_images_view import images_view_layout
from pages.markup.layout_markup import markup_layout

dash.register_page('start', path='/', layout=start_layout)
dash.register_page('login', path='/login', layout=login_layout)
dash.register_page('signup', path='/signup', layout=signup_layout)
dash.register_page('images_view', path='/images_view', layout=images_view_layout)
dash.register_page('markup', path='/markup', layout=markup_layout)

app.layout = html.Div([
    dash.page_container
])


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=7004)


