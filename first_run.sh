#!/bin/bash

docker pull pytorch/pytorch
docker pull styriadigital/postgres_hstore

docker build -t db:7 ./base/
docker build -t front:7 ./front/