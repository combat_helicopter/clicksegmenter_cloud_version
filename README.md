1. Зайди в /clicksegmenter_cloud_version/front/front_info/code/config.py и установи переменные:
* CLASSES - список размечаемых классов
* DISK_REAL_FULL_IMAGES_PATH - папка с картинками, которые будешь размечать

2. Запусти ./first_run.sh - он создаст нужные образы

3. Запусти ./second_run.sh - он запустить разметчик

4. Напиши в браузере http://127.0.0.1:7004/

5. Начинай разметку

6. В папку /clicksegmenter_cloud_version/front/front_info/disks/disks будут складываться маски объектов

7. Для завершения работы нажмите в командной строке ctrl + C 

8. После первого запуска войдите в файл /clicksegmenter_cloud_version/front/front_info/code/config.py и установите:
* RESET_ALL = False (Это необходимо чтобы не затирать предыдущую историю)

9. При повторных запусках следует вызывать только файл ./second_run
