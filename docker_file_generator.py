import os
from copy import copy
from front.front_info.code.config import DISK_REAL_FULL_IMAGES_PATH

current_dir = os.getcwd()
disk_dir = os.path.join(current_dir, 'front/front_info/disks')
code_dir = os.path.join(current_dir, 'front/front_info/code')

docker_compose_str = f"""
version: '2.1'
services:
        front-server:
                image: front:7
                volumes: 
                        - '{disk_dir}:/usr/src/app/disks'
                        - '{code_dir}:/usr/src/app/code'
                        - '{DISK_REAL_FULL_IMAGES_PATH}:/usr/src/app/real_full_images'
                environment:
                        - DISKS=/usr/src/app/disks/
                        - REAL_FULL_IMAGES_PATH=/usr/src/app/real_full_images
                        - TOKEN=AQVN0QI5XTbI9oJH_C-RFAnE387yu07TT3RBV2pI
                        - NODE_ID=bt1dj17qb3uesnj9cr8i
                        - FOLDER_ID=b1gbm9skjpv4gt0r8dmi
                        - MODEL_ID=bt14q1u30qkkbosh55ck
                        - POSTGRES_PASSWORD=1234
                        - POSTGRES_USER=admin
                        - POSTGRES_DB=db_auth
                        - DB_HOST=172.7.0.3
                        - DB_PORT=7003
                        - PATCH_SIZE=350
                ports:
                        - '7004:7004'
                restart: unless-stopped
                networks: 
                        front-model-db-network:
                                ipv4_address: 172.7.0.4
                depends_on:
                        - db-auth

        db-auth:
                image: db:7
                environment:
                        - POSTGRES_PASSWORD=1234
                        - POSTGRES_USER=admin
                        - POSTGRES_DB=db_auth
                ports: 
                        - '7003:7003'
                networks:
                        front-model-db-network:
                                ipv4_address: 172.7.0.3

networks:
        front-model-db-network:
                name: front-model-db-network-v7
                driver: bridge
                ipam:
                        config:
                                - subnet: 172.7.0.0/16
                                  gateway: '172.7.0.1'
"""

with open('./docker-compose.yml', 'w') as docker_compose_file:
    docker_compose_file.write(docker_compose_str)